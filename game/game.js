class Vector {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  plus(objVector) {
    if (!(objVector instanceof Vector)) {
      throw new Error('Можно прибавлять к вектору только вектор типа Vector');
    }
    return new Vector(this.x + objVector.x, this.y + objVector.y);
  }

  times(multiplier) {
    return new Vector(this.x * multiplier, this.y * multiplier);
  }
}

class Actor {
  constructor(vecPos = new Vector(0, 0), vecSize = new Vector(1, 1), vecSpeed = new Vector(0, 0)) {
    if (!(vecPos instanceof Vector) || !(vecSize instanceof Vector) || !(vecSpeed instanceof Vector)) {
      throw new Error('Можно передать только объект типа Vector');
    }
    this.pos = vecPos;
    this.size = vecSize;
    this.speed = vecSpeed;
  } 

  get left() {
    return this.pos.x;
  }

  get right() {
    return this.pos.x + this.size.x;
  }

  get top() {
    return this.pos.y;
  }

  get bottom() {
    return this.pos.y + this.size.y;
  }

  get type() {
    return 'actor';
  }

  act() {

  }

  isIntersect(objActor) {
    if (!(objActor instanceof Actor)) {
      throw new Error('Можно передать только объект типа Actor');
    }
    if (objActor === this) {
        return false;
    }
    return objActor.left < this.right && objActor.right > this.left && objActor.top < this.bottom && objActor.bottom > this.top;
  }
}


class Level {
  constructor(grid = [], actors = []) {
    this.grid = grid;
    this.actors = actors;
    this.player = actors.find(actor => actor.type === "player");
    this.height = grid.length;
    this.width = grid.reduce((max, arr) => max > arr.length ? max : arr.length, 0);
    this.status = null;
    this.finishDelay = 1;
  }

  isFinished() {
    return this.status !== null && this.finishDelay < 0;
  }

  actorAt(moveActor) {
    if (!(moveActor instanceof Actor)) {
      throw new Error('Не являеся объектом типа Actor');
    }
    return this.actors.find(act => act.isIntersect(moveActor));
  }

  obstacleAt(dirObj, sizeObj) {
    if (!(dirObj instanceof Vector) || !(sizeObj instanceof Vector)) {
      throw 'Не являеся объектом типа Actor';
    }

    const leftBorder = Math.floor(dirObj.x);
    const topBorder = Math.floor(dirObj.y);
    const rightBorder = Math.ceil(dirObj.x + sizeObj.x);
    const bottomBorder = Math.ceil(dirObj.y + sizeObj.y);

    if (leftBorder < 0 || rightBorder > this.width || topBorder < 0) {
      return "wall";
    }

    if (bottomBorder > this.height) {
      return "lava";
    }

    for (let y = topBorder; y < bottomBorder; y++) {
      for (let x = leftBorder; x < rightBorder; x++) {
        let currentCord = this.grid[y][x];
        if (currentCord) {
          return currentCord;
        }
      }  
    }
  }

  removeActor(removeObj) {
    const indexOfActorObject = this.actors.indexOf(removeObj);
    if (indexOfActorObject !== -1) {
      this.actors.splice(indexOfActorObject, 1);
    }
  }

  noMoreActors(strType) {
    return !this.actors.find(actor => actor.type === strType);
  }

  playerTouched(typeBarrier, movingObject) {
    if (typeBarrier === 'lava' || typeBarrier === 'fireball') {
      this.status = 'lost';
    }

    if (typeBarrier === 'coin' || (movingObject instanceof Actor)) {
      this.removeActor(movingObject);
      if (this.noMoreActors('coin')) {
        this.status = 'won';
      }
    }
  }
}

class LevelParser {
  constructor(dictionary = {}) {
    this.dictionary = dictionary;
  }

  actorFromSymbol(str) {
      return this.dictionary[str];
  }

  obstacleFromSymbol(str) {
    switch (str) {
      case 'x':
        return 'wall';
      case '!':
        return 'lava';
    }
  }

  createGrid(arrStr) {
    return arrStr.map(line => line.split('')).map(line => line.map(line => this.obstacleFromSymbol(line)));
  }

  createActors(arrStr) {
    return arrStr.reduce((prev, rowY, y) => {
      rowY.split('').forEach( (rowX, x) => {
          let constructor = this.actorFromSymbol(rowX);
          if (typeof constructor === 'function') {
            let actor = new constructor(new Vector(x, y));
            if (actor instanceof Actor) {
              prev.push(actor);
            }
          }
      });
      return prev;
    }, []);
  }

  parse(arrStr) {
    return new Level(this.createGrid(arrStr), this.createActors(arrStr));
  }
}

class Fireball extends Actor {
  constructor(vecPos = new Vector(0, 0), vecSpeed = new Vector(0, 0)) {
    const vecSize = new Vector(1, 1);
    super(vecPos, vecSize, vecSpeed);
  }

  get type() {
    return 'fireball';
  }

  getNextPosition(time = 1) {
    return this.pos.plus(this.speed.times(time))
  }

  handleObstacle() {
    this.speed = this.speed.times(-1);
  }

  act(time, field) {
    if ((field.obstacleAt(this.getNextPosition(time), this.size))) {
      this.handleObstacle();
    } else {
      this.pos = this.getNextPosition(time);
    }
  }


}

class HorizontalFireball extends Fireball {
  constructor(vecPos) {
    const vecSpeed = new Vector(2, 0);
    super(vecPos, vecSpeed);
  }
}

class VerticalFireball extends Fireball {
  constructor(vecPos) {
    const vecSpeed = new Vector(0, 2);
    super(vecPos, vecSpeed);
  }
}

class FireRain extends Fireball {
  constructor(vecPos) {
    const vecSpeed = new Vector(0, 3);
    super(vecPos, vecSpeed);
    this.start = vecPos;
  }

  handleObstacle() {
    this.pos = this.start;
  }
}

class Coin extends Actor {
  constructor(vecPos = new Vector(0, 0)) {
    vecPos = vecPos.plus(new Vector(0.2, 0.1));
    const vecSize = new Vector(0.6, 0.6);
    super(vecPos, vecSize);
    this.oldPos = this.pos;
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.random() * 2 * Math.PI;
  }

  get type() {
    return 'coin';
  }

  updateSpring(time = 1) {
    this.spring += this.springSpeed * time;
  }

  getSpringVector() {
    return new Vector(0, Math.sin(this.spring) * this.springDist);
  }

  getNextPosition(time = 1) {
    this.updateSpring(time);
    return this.oldPos.plus(this.getSpringVector());
  }

  act(time) {
    this.pos = this.getNextPosition(time);
  }
}

class Player extends Actor {
  constructor(vecPos = new Vector(0, 0)) {
    vecPos = vecPos.plus(new Vector(0, -0.5));
    const vecSize = new Vector(0.8, 1.5);
    const vecSpeed = new Vector(0, 0);
    super(vecPos, vecSize, vecSpeed);
  }

  get type() {
    return 'player';
  }
}

loadLevels()
  .then(JSON.parse)
  .then(shemas => runGame(shemas, parser, DOMDisplay)
    .then(() => alert('Вы выиграли приз!')));

const actorDict = {
  '@': Player,
  'v': FireRain,
  'o': Coin,
  '=': HorizontalFireball,
  '|': VerticalFireball
}


const parser = new LevelParser(actorDict);